package uk.co.testforce.framework.excel;

import uk.co.testforce.framework.properties.ExecutionContext;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public abstract class TestData {
    protected static final String HIERARCHY_SEPARATOR = ExecutionContext.getInstance().getConfigData().getProperty("Data", new String[]{"HierarchySeparator"});
    protected List<String> filePaths = new LinkedList();

    public TestData() {
    }

    protected abstract void load(File var1, String var2, String var3);

    public abstract String getInputValue(String var1, String... var2);

    public abstract String getExpectedResult(String var1, String... var2);

    public abstract String[] getInputValueKeys();

    public abstract String[] getExpectedResultKeys();

    public final boolean getBooleanExpectedResult(String key, String... subkeys) {
        return Boolean.parseBoolean(this.getExpectedResult(key, subkeys));
    }

    public final byte getByteExpectedResult(String key, String... subkeys) {
        return Byte.parseByte(this.getExpectedResult(key, subkeys));
    }

    public final double getDoubleExpectedResult(String key, String... subkeys) {
        return Double.parseDouble(this.getExpectedResult(key, subkeys));
    }

    public final float getFloatExpectedResult(String key, String... subkeys) {
        return Float.parseFloat(this.getExpectedResult(key, subkeys));
    }

    public final int getIntExpectedResult(String key, String... subkeys) {
        return Integer.parseInt(this.getExpectedResult(key, subkeys));
    }

    public final long getLongExpectedResult(String key, String... subkeys) {
        return Long.parseLong(this.getExpectedResult(key, subkeys));
    }

    public final short getShortExpectedResult(String key, String... subkeys) {
        return Short.parseShort(this.getExpectedResult(key, subkeys));
    }

    public final boolean getBooleanInputValue(String key, String... subkeys) {
        return Boolean.parseBoolean(this.getInputValue(key, subkeys));
    }

    public final byte getByteInputValue(String key, String... subkeys) {
        return Byte.parseByte(this.getInputValue(key, subkeys));
    }

    public final double getDoubleInputValue(String key, String... subkeys) {
        return Double.parseDouble(this.getInputValue(key, subkeys));
    }

    public final float getFloatInputValue(String key, String... subkeys) {
        return Float.parseFloat(this.getInputValue(key, subkeys));
    }

    public final int getIntInputValue(String key, String... subkeys) {
        return Integer.parseInt(this.getInputValue(key, subkeys));
    }

    public final long getLongInputValue(String key, String... subkeys) {
        return Long.parseLong(this.getInputValue(key, subkeys));
    }

    public final short getShortInputValue(String key, String... subkeys) {
        return Short.parseShort(this.getInputValue(key, subkeys));
    }

    public String toString(String inputValuesLabel, String inputValues, String expectedResultsLabel, String expectedValues) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Test Data Files");
        buffer.append('\n');
        buffer.append(this.filePaths);
        buffer.append('\n');
        buffer.append(inputValuesLabel);
        buffer.append('\n');
        buffer.append(inputValues);
        buffer.append('\n');
        buffer.append(expectedResultsLabel);
        buffer.append('\n');
        buffer.append(expectedValues);
        return buffer.toString();
    }

    public void add(TestData newTestData) {
        this.filePaths.addAll(newTestData.filePaths);
    }
}