package uk.co.testforce.framework.excel;

import uk.co.testforce.framework.properties.ExecutionContext;

import java.io.File;

public class TestDataLoader {
    private TestDataLoader() {
    }

    protected static String getDataSourceFilename(String testDataSource) {
        String dataDir = ExecutionContext.getInstance().getConfigData().getProperty("Data", new String[]{"Dir"});
        if (dataDir == null) {
            dataDir = ".";
        }

        return dataDir + File.separatorChar + testDataSource;
    }

    protected static TestData loadDataFromFile(TestData testData, String testDataSource, String key, String value) {
        String dataType = ExecutionContext.getInstance().getConfigData().getProperty("Data", new String[]{"Format"});
        TestData newTestData = null;
        if (dataType != null && (dataType.equalsIgnoreCase("xls") || dataType.equalsIgnoreCase("xlsx") || dataType.equalsIgnoreCase("Excel"))) {
            newTestData = new XLSTestData(getDataSourceFilename(testDataSource), key, value);
        }

        if (testData == null) {
            testData = newTestData;
        } else {
            ((TestData) testData).add(newTestData);
        }

        return (TestData) testData;
    }

    public static TestData load(TestData testData, String testDataSources, String key, String value) {
        String[] testDataSourcesArray = testDataSources.split(",");
        if (testDataSourcesArray.length == 1) {
            return loadDataFromFile(testData, testDataSources, key, value);
        } else {
            String[] arr$ = testDataSourcesArray;
            int len$ = testDataSourcesArray.length;
            for (int i$ = 0; i$ < len$; ++i$) {
                String testDataSource = arr$[i$];
                testData = loadDataFromFile(testData, testDataSource, key, value);
            }

            return testData;
        }
    }

    public static TestData load(String testDataSources, String key, String value) {
        return testDataSources != null ? load((TestData) null, testDataSources, key, value) : null;
    }
}
