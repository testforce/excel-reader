package uk.co.testforce.framework.excel;

import uk.co.testforce.framework.properties.ExecutionContext;
import uk.co.testforce.framework.utils.MapUtils;
import uk.co.testforce.framework.utils.StringUtils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class XLSTestData extends TestData {
    private static final String SHEET_EXPECTED_RESULTS = ExecutionContext.getInstance().getConfigData().getProperty("Data", new String[]{"ExpectedWorksheet"});
    private static final String SHEET_INPUT_VALUES = ExecutionContext.getInstance().getConfigData().getProperty("Data", new String[]{"InputWorksheet"});
    protected Map<String, String> expectedResults = new HashMap();
    protected Map<String, String> inputValues = new HashMap();

    public XLSTestData(String filename, String key, String value) {
        File xlsFile = new File(filename + ".xls");
        if (!xlsFile.isFile()) {
            xlsFile = new File(filename + ".xlsx");
        }

        if (xlsFile.isFile()) {
            this.load(xlsFile, key, value);
        }

    }

    public XLSTestData(String filename) {
        File xlsFile = new File(filename + ".xls");
        if (!xlsFile.isFile()) {
            xlsFile = new File(filename + ".xlsx");
        }

        if (xlsFile.isFile()) {
            this.load(xlsFile);
        }

    }

    public String getInputValue(String key, String... subkeys) {
        return (String) this.inputValues.get(StringUtils.concatWith(HIERARCHY_SEPARATOR, key, subkeys));
    }

    public String getExpectedResult(String key, String... subkeys) {
        return (String) this.expectedResults.get(StringUtils.concatWith(HIERARCHY_SEPARATOR, key, subkeys));
    }

    protected void load(File testDataFile, String key, String value) {
        this.filePaths.add(testDataFile.getAbsolutePath());
        XLSReader xlsReader = new XLSReader(testDataFile);
        xlsReader.load(SHEET_INPUT_VALUES, this.inputValues, key, value);
        xlsReader.load(SHEET_EXPECTED_RESULTS, this.expectedResults, key, value);
    }

    public String[] getInputValueKeys() {
        return MapUtils.getKeys(this.inputValues);
    }

    public String[] getExpectedResultKeys() {
        return MapUtils.getKeys(this.expectedResults);
    }

    public void add(TestData newTestData) {
        if (newTestData != null) {
            super.add(newTestData);
            String[] newInputValues = newTestData.getInputValueKeys();
            String[] newExpectedResults = newInputValues;
            int len$ = newInputValues.length;

            for (int i$ = 0; i$ < len$; ++i$) {
                String newInputValue = newExpectedResults[i$];
                this.inputValues.put(newInputValue, newTestData.getInputValue(newInputValue, new String[0]));
            }

            newExpectedResults = newTestData.getExpectedResultKeys();
            String[] arr$ = newExpectedResults;
            len$ = newExpectedResults.length;

            for (int i$ = 0; i$ < len$; ++i$) {
                String newExpectedResult = arr$[i$];
                this.expectedResults.put(newExpectedResult, newTestData.getExpectedResult(newExpectedResult, new String[0]));
            }
        }

    }

    public String toString() {
        return super.toString(SHEET_INPUT_VALUES, this.inputValues.toString(), SHEET_EXPECTED_RESULTS, this.expectedResults.toString());
    }

    protected void load(File testDataFile) {
        this.filePaths.add(testDataFile.getAbsolutePath());
        XLSReader xlsReader = new XLSReader(testDataFile);
        xlsReader.loadMultipleLines(SHEET_INPUT_VALUES, this.inputValues, (String) null, (String) null);
        xlsReader.loadMultipleLines(SHEET_EXPECTED_RESULTS, this.expectedResults, (String) null, (String) null);
    }
}
