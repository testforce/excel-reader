package uk.co.testforce.framework.properties;

import java.util.*;

public class AllPropertiesLoader extends ConfigData {
    private List<ConfigData> configDataList;

    public AllPropertiesLoader(ConfigData... configData) {
        this.configDataList = new ArrayList(Arrays.asList(configData));
    }

    public Collection<String> getKeys() {
        ArrayList keys = new ArrayList();
        Iterator var2 = this.configDataList.iterator();

        while (var2.hasNext()) {
            ConfigData data = (ConfigData) var2.next();
            keys.addAll(data.getKeys());
        }

        return keys;
    }

    public String getProperty(String key, String... subkeys) {
        String property = null;
        Iterator var4 = this.configDataList.iterator();

        while (var4.hasNext()) {
            ConfigData data = (ConfigData) var4.next();
            if (data.getProperty(key, subkeys) != null) {
                property = data.getProperty(key, subkeys);
            }
        }

        return property;
    }

    protected void loadConfigData(String filePath) {
    }
}

