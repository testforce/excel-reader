package uk.co.testforce.framework.properties;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class ConfigDataLoader {

    private static final String PROPERTY_CONFIG_DATA_FILE_PATH = "CONFIG_DATA_FILE_PATH";
    private static final String DEFAULT_CONFIG_DATA_FILE_PATH = "config.properties";
    private static final String PROPERTY_ENV_DATA_FILE_PATH = "ENVIRONMENT_DATA_FILE_PATH";
    private static final String DEFAULT_ENV_DATA_FILE_PATH = "environment.properties";
    private static final String PROPERTY_ADDITIONAL_CONFIG_FILES = "ADDITIONAL_CONFIG_FILES";
    private static final String ADDITIONAL_CONFIG_FILES_SEPARATOR = ",";
    private static List<String> configFiles;

    private ConfigDataLoader() {
    }

    private static List<String> getConfigFilesList() {
        if (configFiles == null) {
            configFiles = new ArrayList();
        }

        return configFiles;
    }

    public static ConfigData load(List<String> files) {
        if (areAllFilesPropertyFiles(files)) {
            return new PropertiesConfigData(files);
        } else {
            throw new IllegalArgumentException("ConfigDataLoader: load(boolean,String,String...): error: unknown file type exists in list: " + files.toString());
        }
    }

    private static boolean areAllFilesPropertyFiles(List<String> files) {
        Iterator var1 = files.iterator();

        String file;
        do {
            if (!var1.hasNext()) {
                return true;
            }

            file = (String) var1.next();
        } while (file.endsWith(".properties") || file.endsWith(".xml"));

        return false;
    }

    public static ConfigData load() {
        String config = getPropertyValue("CONFIG_DATA_FILE_PATH", "src/test/resources/config.properties");
        String environment = getPropertyValue("ENVIRONMENT_DATA_FILE_PATH", "environment.properties");
        System.out.println(config + " ==== " + environment);
        addConfigFilesToList(new String[]{config, environment});
        String additionalConfigFiles = System.getProperty("ADDITIONAL_CONFIG_FILES");
        if (additionalConfigFiles != null) {
            addConfigFilesToList(additionalConfigFiles.split(","));
        }

        return new AllPropertiesLoader(new ConfigData[]{load(getConfigFilesList()), new SystemPropertiesData()});
    }

    private static String getPropertyValue(String property, String defaultValue) {
        String propertyValue = System.getProperty(property);
        return propertyValue != null ? propertyValue : defaultValue;
    }

    private static void addConfigFilesToList(String... filePaths) {
        String[] var1 = filePaths;
        int var2 = filePaths.length;

        for (int var3 = 0; var3 < var2; ++var3) {
            String file = var1[var3];
            if (ConfigDataLoader.class.getClassLoader().getResource(file) != null || (new File(file)).exists()) {
                getConfigFilesList().add(file);
            }
        }

    }
}

