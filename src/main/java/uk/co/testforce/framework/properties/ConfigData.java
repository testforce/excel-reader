package uk.co.testforce.framework.properties;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public abstract class ConfigData {
    private static final String DEFAULT_KEYWORD_HIERARCHY_SEPARATOR = ".";
    protected final String KEYWORD_HIERARCHY_SEPARATOR;

    public ConfigData() {
        String s = System.getenv("CONFIG_DATA_KEYWORD_HIERARCHY_SEPARATOR");
        this.KEYWORD_HIERARCHY_SEPARATOR = s == null ? "." : s;
    }

    public ConfigData(List<String> filepaths) {
        String hierarchySeparator = System.getenv("CONFIG_DATA_KEYWORD_HIERARCHY_SEPARATOR");
        this.KEYWORD_HIERARCHY_SEPARATOR = hierarchySeparator == null ? "." : hierarchySeparator;
        Iterator var3 = filepaths.iterator();

        while (var3.hasNext()) {
            String file = (String) var3.next();
            this.loadConfigData(file);
        }

    }

    protected abstract void loadConfigData(String var1);

    public abstract Collection<String> getKeys();

    public abstract String getProperty(String var1, String... var2);

    public final int getIntProperty(String key, String... subkeys) {
        return Integer.parseInt(this.getProperty(key, subkeys));
    }

    public final long getLongProperty(String key, String... subkeys) {
        return Long.parseLong(this.getProperty(key, subkeys));
    }

    public final boolean getBooleanProperty(String key, String... subkeys) {
        return Boolean.parseBoolean(this.getProperty(key, subkeys));
    }

    public final String getStringProperty(String key, String... subkeys) {
        return this.getProperty(key, subkeys);
    }
}

