package uk.co.testforce.framework.properties;

import uk.co.testforce.framework.utils.StringUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;

public class SystemPropertiesData extends ConfigData {
    protected Properties configData = System.getProperties();

    public SystemPropertiesData() {
    }

    public Collection<String> getKeys() {
        HashSet result = new HashSet();
        Iterator var2 = this.configData.keySet().iterator();

        while (var2.hasNext()) {
            Object key = var2.next();
            result.add(key.toString());
        }

        return result;
    }

    public String getProperty(String key, String... subkeys) {
        return this.configData.getProperty(StringUtils.concatWith(this.KEYWORD_HIERARCHY_SEPARATOR, key, subkeys));
    }

    protected void loadConfigData(String filePath) {
    }
}

