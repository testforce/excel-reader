package uk.co.testforce.framework.properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;

public class BaseRunner {

    public BaseRunner() {
    }

    @BeforeClass
    public static final void baseRunnerSetup() {
        ExecutionContext.getInstance().setConfigData(ConfigDataLoader.load());
    }

    @AfterClass
    public static final void baseRunnerTearDown() {
    }

}
