package uk.co.testforce.framework.utils;

public class StringUtils {
    private StringUtils() {
    }

    public static String concatWith(String separator, String first, String... rest) {
        StringBuffer result = new StringBuffer(first);
        String[] arr$ = rest;
        int len$ = rest.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            String element = arr$[i$];
            result.append(separator);
            result.append(element);
        }

        return result.toString();
    }
}
