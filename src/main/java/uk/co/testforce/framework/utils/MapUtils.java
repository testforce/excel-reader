package uk.co.testforce.framework.utils;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class MapUtils {
    public MapUtils() {
    }

    public static String[] getKeys(Map<String, String> map) {
        int index = 0;
        String[] keys = new String[map.size()];

        for (Iterator i$ = map.entrySet().iterator(); i$.hasNext(); ++index) {
            Entry<String, String> mapEntry = (Entry) i$.next();
            keys[index] = (String) mapEntry.getKey();
        }

        return keys;
    }
}